package net.rubright.edtest.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CommonServiceImpl implements  CommonService {

    @Value("${test.common.message1:Message1 from CommonServiceImpl(default).}")
    String message1;

    @Autowired
    private InternalService internalService;

    @Override
    public String getMessage1() {
        return message1;
    }

    @Override
    public String getMessage2() {
        return internalService.getMessage2();
    }
}
