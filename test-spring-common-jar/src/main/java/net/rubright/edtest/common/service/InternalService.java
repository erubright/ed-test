package net.rubright.edtest.common.service;

import net.rubright.edtest.common.entity.Entity1;

public interface InternalService {

    String getMessage2();

    Entity1 getEntity(Integer id);

    String getSiteOptimizerVersion();
}
