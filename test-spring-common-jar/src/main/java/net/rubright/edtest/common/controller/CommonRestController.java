package net.rubright.edtest.common.controller;

import net.rubright.edtest.common.ScheduledTasks;
import net.rubright.edtest.common.entity.Entity1;
import net.rubright.edtest.common.service.InternalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import net.rubright.edtest.common.service.CommonService;

@RestController
@RequestMapping("/common")
public class CommonRestController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private InternalService internalService;

    @Autowired
    private ScheduledTasks scheduledTasks;

    @RequestMapping("/test1")
    @ResponseBody
    public String test1() {
        return commonService.getMessage1();
    }

    @RequestMapping("/message2")
    @ResponseBody
    public String message2() {
        return commonService.getMessage2();
    }

    @RequestMapping("/test2")
    @ResponseBody
    public String test2() {
        return internalService.getMessage2();
    }

    @RequestMapping("/entity/{id}")
    @ResponseBody
    public Entity1 getEntity(@PathVariable Integer id) {
        return internalService.getEntity(id);
    }

    @RequestMapping("/siteoptver")
    @ResponseBody
    public String getSiteOptVersion() {
        return internalService.getSiteOptimizerVersion();
    }

    @RequestMapping(value = "/timetask/{interval}", method = RequestMethod.POST)
    @ResponseBody
    public void setTimeTaskInterval(@PathVariable int interval) {
        scheduledTasks.setInterval(interval);
    }

}
