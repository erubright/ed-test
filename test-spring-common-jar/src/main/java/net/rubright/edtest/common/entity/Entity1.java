package net.rubright.edtest.common.entity;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

public class Entity1 {
    private Date now;
    private String message;
    private UUID uuid;

    public Entity1(String message) {
        this.now = Date.from(Instant.now());
        this.message = message;
        this.uuid = UUID.randomUUID();
    }

    public Date getNow() {
        return now;
    }

    public void setNow(Date now) {
        this.now = now;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Entity1{");
        sb.append("now=").append(now);
        sb.append(", message='").append(message).append('\'');
        sb.append(", uuid=").append(uuid);
        sb.append('}');
        return sb.toString();
    }
}
