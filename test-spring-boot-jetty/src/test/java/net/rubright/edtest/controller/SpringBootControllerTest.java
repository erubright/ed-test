package net.rubright.edtest.controller;

import net.rubright.edtest.testjetty.SpringBootJetty;
import net.rubright.edtest.testjetty.entity.HelloWorld;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;

import static org.assertj.core.api.Assertions.*;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SpringBootJetty.class)
public class SpringBootControllerTest {

    @LocalServerPort
    int serverPort;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test1() {
        URI uri = URI.create("http://localhost:"+serverPort+"/hw");

        ResponseEntity<HelloWorld> responseEntity = restTemplate.getForEntity(uri, HelloWorld.class);
        System.err.println(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        HelloWorld response = responseEntity.getBody();
        System.err.println(response);
    }

    @Test
    public void test2() {
        URI uri = URI.create("http://localhost:"+serverPort+"/hw_exception");

        ResponseEntity<HelloWorld> responseEntity = restTemplate.getForEntity(uri, HelloWorld.class);
        System.err.println(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        HelloWorld response = responseEntity.getBody();
        System.err.println(response);
    }

    @Test
    public void test_helloworld2() {
        URI uri = URI.create("http://localhost:"+serverPort+"/jaxrs/helloworld2");

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);
        System.err.println(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        String response = responseEntity.getBody();
        System.err.println(response);
        assertThat(response).isEqualTo("helloWorld2");
    }

    @Test
    public void test_helloworld2_Except() {
        URI uri = URI.create("http://localhost:"+serverPort+"/jaxrs/helloworld2Except");

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);
        System.err.println(responseEntity);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        String response = responseEntity.getBody();
        System.err.println(response);
//        assertThat(response).isEqualTo("helloWorld2");
    }

}
