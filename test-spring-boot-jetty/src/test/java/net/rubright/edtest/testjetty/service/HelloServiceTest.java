package net.rubright.edtest.testjetty.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class HelloServiceTest {

    @Test
    public void test_getMessage() {
        HelloService helloService = new HelloServiceImpl();
        assertThat(helloService.getMessage()).isEqualTo("Hello World!");
    }
}
