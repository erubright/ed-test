package net.rubright.edtest.testjetty;

import net.rubright.edtest.testjetty.entity.HelloWorld;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class SpringBootJettyTest {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootJettyTest.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testString() throws Exception {
        ResponseEntity<String> entity = this.restTemplate.getForEntity("/test", String.class);
        logger.info(entity.toString());
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody()).isEqualTo("Hello World!");
    }


    @Test
    public void testEntity() throws Exception {
        ResponseEntity<HelloWorld> entity = this.restTemplate.getForEntity("/hw", HelloWorld.class);
        logger.info(entity.toString());
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
