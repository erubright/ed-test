package net.rubright.edtest.testjetty.service;

import net.rubright.edtest.testjetty.entity.HelloWorld;

public interface HelloService {

    String getMessage();

    HelloWorld getHelloWorld();

    HelloWorld getHW() throws Exception;
}
