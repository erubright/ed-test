package net.rubright.edtest.testjetty.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface JerseyController2 {
    @GET
    @Path("/helloworld2")
    String helloWorld2();

    @GET
    @Path("/helloworld2Except")
    String helloWorldExcept() throws Exception;
}
