package net.rubright.edtest.testjetty.controller;

import net.rubright.edtest.testjetty.entity.HelloWorld;
import net.rubright.edtest.testjetty.service.HelloService;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jersey.ResourceConfigCustomizer;
import org.springframework.stereotype.Component;

@Component
public class JerseyControllerImpl implements JerseyController,ResourceConfigCustomizer {

    @Autowired
    private HelloService helloService;

    @Override
    public String helloWorld() {
        return helloService.getMessage();
    }


    @Override
    public HelloWorld getHelloWorld() {
        return helloService.getHelloWorld();
    }

    @Override
    public void customize(ResourceConfig resourceConfig) {
        resourceConfig.register(this);
    }
}
