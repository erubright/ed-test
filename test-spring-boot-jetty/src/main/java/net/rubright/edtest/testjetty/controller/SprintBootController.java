package net.rubright.edtest.testjetty.controller;

import net.rubright.edtest.testjetty.entity.HelloWorld;
import net.rubright.edtest.testjetty.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SprintBootController {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/test")
    public String helloWorld() {
        return helloService.getMessage();
    }

    @RequestMapping(value = "/hw",method = RequestMethod.GET)
    public HelloWorld getHelloWorld() {
        return helloService.getHelloWorld();
    }

    @RequestMapping(value = "/hw_exception",method = RequestMethod.GET)
    public HelloWorld getHW() throws Exception {
        return helloService.getHW();
    }
}
