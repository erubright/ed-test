package net.rubright.edtest.testjetty.controller;

import net.rubright.edtest.testjetty.entity.HelloWorld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface JerseyController {
    @GET
    @Path("/helloworld")
    String helloWorld();


    @GET
    @Path("/hw")
    HelloWorld getHelloWorld();
}
