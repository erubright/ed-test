package net.rubright.edtest.testjetty.service;

import net.rubright.edtest.testjetty.entity.HelloWorld;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {

    private String message;

    public HelloServiceImpl() {
        this("Hello World!");
    }
    public HelloServiceImpl(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HelloWorld getHelloWorld() {
        return new HelloWorld(message);
    }

    @Override
    public HelloWorld getHW() throws Exception {
        throw new Exception("getHW() threw Exception!");
    }
}
