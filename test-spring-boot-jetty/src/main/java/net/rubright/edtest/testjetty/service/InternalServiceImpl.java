package net.rubright.edtest.testjetty.service;

import net.rubright.edtest.common.entity.Entity1;
import net.rubright.edtest.common.service.InternalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InternalServiceImpl implements InternalService {
    private static final Logger logger = LoggerFactory.getLogger(InternalService.class);

    @Autowired
    @Qualifier("restTemplateSiteOpt")
    private RestTemplate restTemplateSiteOpt;

    @Override
    public String getMessage2() {
        return "Message 2 from InternalServiceImpl.";
    }

    @Override
    public Entity1 getEntity(Integer id) {
        return new Entity1("Entity"+id+" created from InternalServiceImpl.");
    }

    @Override
    public String getSiteOptimizerVersion() {
        return restTemplateSiteOpt.getForObject("http://localhost:12113/rest/version", String.class);
    }

}
