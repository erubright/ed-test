package net.rubright.edtest.testjetty.entity;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

public class HelloWorld {
    private Date now;
    private String message;
    private UUID uuid;

    public HelloWorld() {}
    public HelloWorld(String message) {
        this.message = message;
        this.now = Date.from(Instant.now());
        this.uuid = UUID.randomUUID();
    }

    public Date getNow() {
        return now;
    }

    public void setNow(Date now) {
        this.now = now;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("HelloWorld{");
        sb.append("now=").append(now);
        sb.append(", message='").append(message).append('\'');
        sb.append(", uuid=").append(uuid);
        sb.append('}');
        return sb.toString();
    }
}
