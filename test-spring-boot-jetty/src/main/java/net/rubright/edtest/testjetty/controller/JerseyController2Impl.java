package net.rubright.edtest.testjetty.controller;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.autoconfigure.jersey.ResourceConfigCustomizer;
import org.springframework.stereotype.Component;

@Component
public class JerseyController2Impl implements JerseyController2,ResourceConfigCustomizer {
    @Override
    public String helloWorld2() {
        return "helloWorld2";
    }

    @Override
    public String helloWorldExcept() throws Exception {
        throw new Exception("Thrown from helloWorldExcept!");
    }

    @Override
    public void customize(ResourceConfig resourceConfig) {
        resourceConfig.register(this);
    }
}
