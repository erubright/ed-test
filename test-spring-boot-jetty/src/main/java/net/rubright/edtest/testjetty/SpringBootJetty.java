package net.rubright.edtest.testjetty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = "net.rubright.edtest")
public class SpringBootJetty {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootJetty.class);

    public static void main(String[] args) throws InterruptedException {
        SpringApplication app = new SpringApplication(SpringBootJetty.class);
        app.setWebEnvironment(true);
        ConfigurableApplicationContext context = app.run(args);
        logger.info("After SpringApplication.run called.");
    }
}
