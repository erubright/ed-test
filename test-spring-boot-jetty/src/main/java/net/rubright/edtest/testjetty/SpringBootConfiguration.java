package net.rubright.edtest.testjetty;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SpringBootConfiguration {

    @Bean("restTemplateSiteOpt")
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.basicAuthorization("sos", "sit30pt!").build();
    }

    @Bean
    public ResourceConfig resourceConfig() {
        return new JerseyConfig();
    }

}
