package net.rubright.edtest.testjetty;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/jaxrs")
public class JerseyConfig extends ResourceConfig{
    public JerseyConfig() {}
}
