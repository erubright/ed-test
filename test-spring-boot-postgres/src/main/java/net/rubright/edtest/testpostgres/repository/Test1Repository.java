package net.rubright.edtest.testpostgres.repository;


import net.rubright.edtest.testpostgres.entity.Test1;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Test1Repository extends CrudRepository<Test1, Long> {
    List<Test1> findByC1(String c1);
}
