package net.rubright.edtest.testpostgres.entity;

import javax.persistence.*;

@Entity
@Table(name="test1")
public class Test1 {
    private static final long serialVersionUID =-1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "c1")
    private String c1;

    @Column(name = "c2")
    private String c2;


    public Test1() {}
    public Test1(String c1, String c2) {
        this.c1 = c1;
        this.c2 = c2;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getC1() {
        return c1;
    }
    public void setC1(String c1) {
        this.c1 = c1;
    }

    public String getC2() {
        return c2;
    }
    public void setC2(String c2) {
        this.c2 = c2;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Test1{");
        sb.append("id=").append(id);
        sb.append(", c1='").append(c1).append('\'');
        sb.append(", c2='").append(c2).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
