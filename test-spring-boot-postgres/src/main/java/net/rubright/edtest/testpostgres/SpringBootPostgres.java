package net.rubright.edtest.testpostgres;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = "net.rubright.edtest")
public class SpringBootPostgres {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootPostgres.class);

    public static void main(String[] args) throws InterruptedException {
        SpringApplication app = new SpringApplication(SpringBootPostgres.class);
        app.setWebEnvironment(true);
        ConfigurableApplicationContext context = app.run(args);
        logger.info("After SpringApplication.run called.");
    }
}
