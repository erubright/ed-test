package net.rubright.edtest.testpostgres.controller;

import net.rubright.edtest.testpostgres.entity.Test1;
import net.rubright.edtest.testpostgres.repository.Test1Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {

    private final Test1Repository repository;

    @Autowired
    public WebController(Test1Repository test1Repository) {
        this.repository = test1Repository;
    }

    @RequestMapping("/save")
    public String process(){
        repository.save(new Test1("Jack", "Smith"));
        repository.save(new Test1("Adam", "Johnson"));
        repository.save(new Test1("Kim", "Smith"));
        repository.save(new Test1("David", "Williams"));
        repository.save(new Test1("Peter", "Davis"));
        return "Done";
    }


    @RequestMapping("/findall")
    public String findAll(){
        String result = "<html>";

        for(Test1 cust : repository.findAll()){
            result += "<div>" + cust.toString() + "</div>";
        }

        return result + "</html>";
    }

    @RequestMapping("/findbyid")
    public String findById(@RequestParam("id") long id){
        String result = "";
        result = repository.findOne(id).toString();
        return result;
    }

    @RequestMapping("/findbylastname")
    public String fetchDataByLastName(@RequestParam("lastname") String lastName){
        String result = "<html>";

        for(Test1 cust: repository.findByC1(lastName)){
            result += "<div>" + cust.toString() + "</div>";
        }

        return result + "</html>";
    }

}
